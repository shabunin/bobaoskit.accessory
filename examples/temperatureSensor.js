const BobaosKit = require("../index");
const Bobaos = require("bobaos.sub");

// bobaos running on raspberry pi
const bobaos = Bobaos({
  redis: "redis://10.0.42.33:6379"
});

// sdk running on local machine
const sdk = BobaosKit.Sdk({
  redis: "redis://127.0.0.1:6379"
});

// TODO: commonAccessory wrapper

// baos datapoint number
const sersor1datapoint = 1;

const sensor1 = BobaosKit.Accessory({
  id: "sensor1",
  name: "Temperature sensor",
  type: "temperature sensor",
  control: [1],
  status: [2],
  sdk: sdk
});

const processOneControlValue = payload => {
  let { field, value } = payload;
  // read request
  if ((field = 1)) {
    return bobaos.readValue(sersor1datapoint);
  }
};
sensor1.on("control accessory value", (payload, cb) => {
  if (Array.isArray(payload)) {
    payload.forEach(processOneControlValue);
  }

  processOneControlValue(payload);
});

const processOneDatapoint = payload => {
  let { id, value } = payload;
  if (id === sersor1datapoint) {
    return sensor1.updateStatusValue({ field: 2, value: value });
  }
};
bobaos.on("datapoint value", payload => {
  if (Array.isArray(payload)) {
    return payload.forEach(processOneDatapoint);
  }

  processOneDatapoint(payload);
});
