const BobaosKit = require("../index");
const Bobaos = require("bobaos.sub");

const bobaos = Bobaos();

const SwitchAccessory = params => {
  let { id, name, controlDatapoint, stateDatapoint } = params;
  const swAcc = BobaosKit.Accessory({
    id: id,
    name: name,
    type: "switch",
    control: ["state"],
    status: ["state"]
  });

  swAcc.on("control accessory value", async (payload, cb) => {
    const processOneValue = async payload => {
      let { field, value } = payload;
      // DO WHAT YOU WANT
      // IN THE END UPDATE STATUS VALUE
      if (field === "state") {
        console.log("cav", value);
        await bobaos.setValue({ id: controlDatapoint, value: value });
      }
    };

    if (Array.isArray(payload)) {
      await Promise.all(payload.map(processOneValue));
      return;
    }

    await processOneValue(payload);
  });
  const processOneValue = async payload => {
    let { id, value } = payload;
    if (id === stateDatapoint) {
      await swAcc.updateStatusValue({ field: "state", value: value });
    }
  };

  bobaos.on("datapoint value", payload => {
    if (Array.isArray(payload)) {
      return payload.forEach(processOneValue);
    }

    return processOneValue(payload);
  });

  return swAcc;
};

const switches = [
  { id: "sw651", name: "WC child", controlDatapoint: 651, stateDatapoint: 652 },
  { id: "sw653", name: "Switch 653", controlDatapoint: 653, stateDatapoint: 653 },
  { id: "sw655", name: "Щитовая", controlDatapoint: 655, stateDatapoint: 656 },
  { id: "sw657", name: "Комната 1", controlDatapoint: 657, stateDatapoint: 658 },
  { id: "sw659", name: "Кинотеатр", controlDatapoint: 659, stateDatapoint: 660 }
];

switches.forEach(SwitchAccessory);
