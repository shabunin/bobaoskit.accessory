// Fake Thermostat example

// this fake thermostat uses random data
// I use it as an example, so it don't send any data to KNX bus
const EE = require("events");

const BobaosKit = require("../index");

// sdk running on local machine
const sdk = BobaosKit.Sdk();

const thermo = BobaosKit.Accessory({
  id: "thermostat_1",
  name: "Fake Thermostat",
  type: "thermostat",
  control: ["mode", "setpoint"],
  status: ["mode", "modes available", "setpoint", "current temperature", "sensors", "status messages", "error state"],
  sdk: sdk
});

const processOneControlValue = payload => {
  let { field, value } = payload;
  // read request
  if (field === "setpoint") {
    return thermo.updateStatusValue({ field: "setpoint", value: value });
  }
  if (field === "mode") {
    // fake mode switch, but give error on heating mode
    if (value === "heat") {
      let statuses = [];
      statuses.push({
        field: "status messages",
        value: ["Something wrong"]
      });
      statuses.push({
        field: "error state",
        value: { error: true, message: "Can't turn on the heater." }
      });
      statuses.push({ field: "mode", value: "none" });
      return thermo.updateStatusValue(statuses);
    }

    if (value === "off") {
      thermo.updateStatusValue({
        field: "error state",
        value: { error: false }
      });
      thermo.updateStatusValue({
        field: "status messages",
        value: ["All devices is off"]
      });
    }
    if (value === "cool") {
      thermo.updateStatusValue({
        field: "error state",
        value: { error: false }
      });
      thermo.updateStatusValue({
        field: "status messages",
        value: ["Air condition is on", "Air conditioner speed is 100%"]
      });
    }
    if (value === "auto") {
      thermo.updateStatusValue({
        field: "error state",
        value: { error: false }
      });
      thermo.updateStatusValue({
        field: "status messages",
        value: ["Something is working", "Something is not"]
      });
    }

    return thermo.updateStatusValue({ field: "mode", value: value });
  }
};

thermo.on("control accessory value", (payload, cb) => {
  console.log("cav", payload);
  if (Array.isArray(payload)) {
    payload.forEach(processOneControlValue);
  }

  processOneControlValue(payload);
});

// fake current temperature
setInterval(_ => {
  thermo.updateStatusValue({ field: "current temperature", value: (Math.random() * 40).toFixed(1) });
}, 30000);

// fake sensors
let sensors = {};
sensors.temperature = 21.0;
sensors.humidity = 36.0;
sensors.co2 = 420;

// fake modes
let modesAvailavle = ["off", "heat", "cool", "auto"];

// every 30 seconds fake data
setInterval(_ => {
  sensors.temperature = (Math.random() * 40).toFixed(1);
  sensors.humidity = (Math.random() * 100).toFixed(1);
  sensors.co2 = (Math.random() * 1000).toFixed(1);
  thermo.updateStatusValue({ field: "sensors", value: sensors });
}, 30000);

// on start send values
thermo.on("ready", async _ => {
  const initialValues = [];
  initialValues.push({ field: "mode", value: "off" });
  initialValues.push({ field: "modes available", value: modesAvailavle });
  initialValues.push({ field: "setpoint", value: 25 });
  initialValues.push({ field: "current temperature", value: 25 });
  initialValues.push({ field: "sensors", value: sensors });
  initialValues.push({ field: "status messages", value: ["accessory just started"] });
  initialValues.push({ field: "error state", value: { error: false } });
  try {
    await thermo.updateStatusValue(initialValues);
  } catch (e) {
    console.log("cannot set initial values", e.message);
  }
});
