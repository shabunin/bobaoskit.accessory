const EE = require("events");

const BobaosKit = require("../index");
const Bobaos = require("bobaos.sub");

// bobaos running on raspberry pi
const bobaos = Bobaos({
  redis: "redis://192.168.4.203:6379"
});

// sdk running on local machine
const sdk = BobaosKit.Sdk({
  redis: "redis://127.0.0.1:6379"
});

// TODO: commonAccessory wrapper

const SoftwareThermo = _ => {
  let self = new EE();
  self._powerState = false;
  self._current = 0;
  self._setpoint = 0;

  self.act = _ => {
    if (self._powerState) {
      if (self._current > self._setpoint) {
        self.emit('out', 0);
      } else {
        self.emit('out', 255);
      }
    } else {
      self.emit('out', 0);
    }
  };
  self.setPower = state => {
    console.log('setPower', state);
    self._powerState = state;
    self.act();
  };
  self.setSetpoint = value => {
    self._setpoint = value;
    self.act();
  };
  self.setCurrent = value => {
    self._current = value;
    self.act();
  };

  return self;
};


// baos datapoint number
const currentTempDatapointStatus =  105;
const setpointTempDatapointControl = 120;
const setpointTempDatapointStatus = 120;
const thermostatOutputControl = 121;
const thermostatOutputStatus = 121;

const mySoftThermo = SoftwareThermo();
mySoftThermo.on('out', value => {
    return bobaos.setValue({
      id: thermostatOutputControl,
      value: value
        });
    });


const thermo = BobaosKit.Accessory({
  id: "thermostat_1",
  name: "Thermostat example",
  type: "thermostat basic",
  control: ["power", "setpoint"],
  status: ["power", "current", "setpoint"],
  sdk: sdk
});

const processOneControlValue = payload => {
  let { field, value } = payload;
  // read request
  if ((field === "setpoint")) {
    return bobaos.setValue({id: setpointTempDatapointControl, value: value});
  }
  if ((field === "power")) {
    // TODO: power thermostat
    mySoftThermo.setPower(value);
  }
};
thermo.on("control accessory value", (payload, cb) => {
  console.log("cav", payload);
  if (Array.isArray(payload)) {
    payload.forEach(processOneControlValue);
  }

  processOneControlValue(payload);
});

const processOneDatapoint = payload => {
  let { id, value } = payload;
  if (id === currentTempDatapointStatus) {
    mySoftThermo.setCurrent(value);
    return thermo.updateStatusValue({ field: "current", value: value });
  }
  if (id === setpointTempDatapointStatus) {
    mySoftThermo.setSetpoint(value);
    return thermo.updateStatusValue({ field: "setpoint", value: value });
  }
};
bobaos.on("datapoint value", payload => {
  if (Array.isArray(payload)) {
    return payload.forEach(processOneDatapoint);
  }

  processOneDatapoint(payload);
});
