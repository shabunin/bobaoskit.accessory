// In order to run properly, install `mympwspawn` and `mympvclient` npm packages
// this script creates simple `radio player` accessory with few radio stations
//

const MpvInstance = require("mympvspawn");
const MpvClient = require("mympvclient");
const BobaosKit = require("../index");

const mpvSocketPath = `${__dirname}/mympv.sock`;
const myMpvInstance = MpvInstance({ socketFile: mpvSocketPath, debug: false }).spawn();
let myMpvClient = MpvClient({ socketFile: mpvSocketPath, debug: false});

myMpvClient.on("error", e => {
  console.log(e.message);
});

let file = "";
const radioList = [
  { id: "relx", name: "Relax FM радио", url: "http://ic3.101.ru:8000/a200" },
  { id: "rock", name: "Rock.FM 95.2 радио", url: "http://nashe1.hostingradio.ru/rock-128.mp3" },
  { id: "kwrk", name: "Kraftwerk", url: "https://www.youtube.com/watch?v=zVeEP5pudD0" },
  { id: "tes3", name: "TESIII: Morrowind", url: "https://www.youtube.com/watch?v=xULTMMgwLuo" },
  { id: "tes4", name: "TESIV: Oblivion", url: "https://www.youtube.com/watch?v=SpqSdORmCX4" },
  { id: "tes5", name: "TESV: Skyrim", url: "https://www.youtube.com/watch?v=TWv3DfoiigM" },
  { id: "hard", name: "Hardbass", url: "https://www.youtube.com/watch?v=f9QmkRHT6Uw" },
  { id: "trol", name: "Trololo", url: "https://www.youtube.com/watch?v=sCNrK-n68CM&t=929s" },
  { id: "hmm4", name: "Heroes IV OST", url: "https://www.youtube.com/watch?v=obK-k848Vto" },
  { id: "entr", name: "Entertainer", url: "https://www.youtube.com/watch?v=0JeGQtdeJDQ" },
  { id: "maf1", name: "Mafia I: The City of Lost Heaven", url: "https://www.youtube.com/watch?v=TgqqftvbtWI" },
  { id: "mogw", name: "Mogwai: Happy Songs For Happy People", url: "https://www.youtube.com/watch?v=83GpuL_wUq4" },
  { id: "pink", name: "Pink Floyd: Wish You Were Here", url: "https://www.youtube.com/watch?v=6FXbF2zoB0A" },
  { id: "sektor", name: "Сектор Газа: сборник", url: "https://www.youtube.com/watch?v=FnWtDdt0xaE" },
  { id: "cool", name: "Casualties of Cool", url: "https://www.youtube.com/watch?v=KXHCrwKU4Qc" }
];

const connectMpv = _ => {
  return new Promise((resolve, reject) => {
    myMpvClient
      .connect()
      .then(_ => {
        console.log(`mpv client connected`);
        resolve();
      })
      .catch(e => {
        console.log(`can't connect to mpv socket, ${e.message}`);
        console.log(`reconnecting`);
        setTimeout(_ => {
          console.log("reconnect timeout handler");
          connectMpv()
            .then(resolve)
            .catch(reject);
        }, 1000);
      });
  });
};

const musicAccessory = BobaosKit.Accessory({
  id: "music42",
  name: "Radio player",
  type: "radio player",
  control: ["playing", "station"],
  status: ["playing", "station", "radio list"]
});

// TODO: update radio list when accessory created
//
musicAccessory.on("ready", _ => {
  connectMpv().then(_ => {
    console.log("hello, friend");
  });
  musicAccessory.updateStatusValue([{ field: "playing", value: false }, { field: "radio list", value: radioList }]);
});

musicAccessory.on("control accessory value", async (method, payload, done) => {
  const processOneValue = async payload => {
    let { field, value } = payload;
    // DO WHAT YOU WANT
    // IN THE END UPDATE STATUS VALUE
    if (field === "playing") {
      await myMpvClient.pause(!value);
      await musicAccessory.updateStatusValue({ field: "playing", value: value });
    }
    if (field === "station") {
      const id = value;
      const findRadioById = t => {
        return t.id === id;
      };
      const foundRadio = radioList.find(findRadioById);
      if (foundRadio) {
        await myMpvClient.stop();
        await myMpvClient.loadfile(foundRadio.url);
        await myMpvClient.pause(false);
        await musicAccessory.updateStatusValue([{ field: "station", value: id }, { field: "playing", value: true }]);
      }
    }
  };

  if (Array.isArray(payload)) {
    try {
      await Promise.all(payload.map(processOneValue));
      return done(null);
    } catch(e) {
      return done(e);
    }
  }

  try {
    await processOneValue(payload);
    done(null);
  } catch(e) {
    return done(e);
  }
});
myMpvClient.on("event", payload => {
  let { event } = payload;
  // console.log("EEEEEEEE", event);
  switch ("event") {
    case "idle":
      musicAccessory.updateStatusValue({ field: "playing", value: false });
      break;
    case "playback-restart":
      musicAccessory.updateStatusValue({ field: "playing", value: true });
      break;
    case "pause":
      musicAccessory.updateStatusValue({ field: "playing", value: false });
      break;
    case "unpause":
      musicAccessory.updateStatusValue({ field: "playing", value: true });
      break;
    default:
      break;
  }
});
