
## Usage:

```js
const BobaosKit = require("bobaoskit.accessory");

const acc1 = BobaosKit.Accessory({
  id: "hello",
  name: "Friend",
  type: "switch",
  control: [1],
  status: [2]
});
acc1.on("control accessory value", async (payload, cb) => {
  const processOneValue = async payload => {
    let {field, value} = payload;
    // DO WHAT YOU WANT
    // IN THE END UPDATE STATUS VALUE
    return acc1.updateStatusValue({field: 2, value: value});
  };

  if (Array.isArray(payload)) {
    await Promise.all(payload.map(processOneValue));
    return;
  }

  await processOneValue(payload);
});
```
