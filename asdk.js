// Client accessory sdk draft
const EE = require("events");
const Queue = require("bee-queue");
const Redis = require("redis");

const AccessorySDK = params => {
  // default
  const _params = {
    redis: undefined,
    job_channel: "bobaoskit_job",
    broadcast_channel: "bobaoskit_bcast"
  };
  Object.assign(_params, params);

  const self = new EE();

  let kitJobs = [];
  const kitQueue = new Queue(_params.job_channel, {
    redis: _params.redis,
    isWorker: false
  });
  kitQueue.on("ready", _ => {
    self.emit("ready");
  });
  kitQueue.on("job succeeded", (id, result) => {
    // hack: sometimes this event is fired before job.save.then
    // so, set timeout to be sure that job save then was called
    setTimeout(_ => {
      const findById = t => t.id === id;
      let found = kitJobs.findIndex(findById);
      if (found > -1) {
        let { method, payload } = result;
        if (method === "success") {
          kitJobs[found].callback(null, payload);
        }
        if (method === "error") {
          kitJobs[found].callback(new Error(payload));
        }
        kitJobs.splice(found, 1);
      } else {
        // there will be jobs from other clients, so do nothing.
        // console.log(`Couldn't find job with id ${id}`);
      }
    }, 1);
  });
  kitQueue.on("job failed", (id, result) => {
    // console.log(`Job ${id} failed with result: ${result}`);
  });

  self.commonRequest = (method, payload) => {
    return new Promise((resolve, reject) => {
      kitQueue
        .createJob({ method: method, payload: payload })
        .save()
        .then(job => {
          let { id } = job;
          let callback = (err, result) => {
            if (err) {
              return reject(err);
            }

            resolve(result);
          };
          kitJobs.push({ id: id, callback: callback });
        })
        .catch(e => {
          reject(e);
        });
    });
  };

  self.ping = _ => {
    return self.commonRequest("ping", null);
  };
  self.getGeneralInfo = _ => {
    return self.commonRequest("get general info", null);
  };
  self.clearAccessories = _ => {
    return self.commonRequest("clear accessories", null);
  };
  self.addAccessory = payload => {
    return self.commonRequest("add accessory", payload);
  };
  self.removeAccessory = payload => {
    return self.commonRequest("remove accessory", payload);
  };
  self.getAccessoryInfo = payload => {
    return self.commonRequest("get accessory info", payload);
  };
  self.getStatusValue = payload => {
    return self.commonRequest("get status value", payload);
  };
  self.updateStatusValue = payload => {
    return self.commonRequest("update status value", payload);
  };
  self.controlAccessoryValue = payload => {
    return self.commonRequest("control accessory value", payload);
  };

  // broadcast events
  const rc = Redis.createClient(_params.redis);
  rc.subscribe(_params.broadcast_channel);
  rc.on("message", (channel, message) => {
    if (channel !== _params.broadcast_channel) {
      return;
    }

    try {
      const { method, payload } = JSON.parse(message);
      setTimeout(_ => {
        self.emit("broadcasted event", method, payload);
      }, 1);
    } catch (e) {
      console.log("Error while parsing incoming message");
    }
  });

  return self;
};

module.exports = AccessorySDK;
