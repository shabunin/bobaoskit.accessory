const EE = require("events");
const Queue = require("bee-queue");
const AccessorySdk = require("./asdk");

const AccessoryInstance = params => {
  // default params
  let _params = {
    id: "id",
    name: "Friendly Name",
    type: "type",
    control: [1],
    status: [2],
    sdk: undefined,
    redis: undefined,
    job_channel: "bobaoskit_job",
    broadcast_channel: "bobaoskit_bcast"
  };
  Object.assign(_params, params);

  const id = _params.id;
  const self = new EE();

  let _sdk;

  // if sdk instance is passed
  if (typeof _params.sdk === "object") {
    _sdk = _params.sdk;
  } else {
    _sdk = AccessorySdk({
      redis: _params.redis,
      job_channel: _params.job_channel,
      broadcast_channel: _params.broadcast_channel
    });
  }

  self.getAccessoryInfo = _ => {
    return _sdk.getAccessoryInfo(id);
  };
  self.getStatusValue = payload => {
    return _sdk.getStatusValue({ id: id, status: payload });
  };
  self.updateStatusValue = payload => {
    return _sdk.updateStatusValue({ id: id, status: payload });
  };

  self.registerAccessory = _ => {
    return _sdk.addAccessory({
      id: id,
      type: _params.type,
      name: _params.name,
      control: _params.control,
      status: _params.status
    });
  };
  self.unregisterAccessory = _ => {
    return _sdk.removeAccessory(id);
  };

  self.initAccessoryQueueWorker = queueId => {
    return new Promise((resolve, reject) => {
      try {
        self._queue = new Queue(queueId, {
          redis: _params.redis,
          isWorker: true,
          getEvents: true,
          stalledInterval: 30000,
          maxStalledCount: 10,
          storeJobs: false,
          removeOnSuccess: true,
          removeOnFailure: true
        });
        // process request to created accessory
        self._queue.process((job, done) => {
          try {
            const { method, payload } = job.data;
            // currently only "control accessory value" request is supported
            if (method === "control accessory value") {
              return self.emit("control accessory value", method, payload, done);
            }

            throw new RangeError("Unknown method");
          } catch (e) {
            return done(e);
          }
        });
        self._queue.on("ready", resolve);
      } catch (e) {
        reject(e);
      }
    });
  };

  const init = async _ => {
    try {
      let info = await self.getAccessoryInfo();
      // if accessory exists
      if (info) {
        await self.unregisterAccessory();
      }
      info = await self.registerAccessory();
      const job_channel = info.job_channel;
      await self.initAccessoryQueueWorker(job_channel);
      self.emit("ready");
    } catch (e) {
      self.emit("error", e);
    }
  };

  init();

  return self;
};

module.exports = {
  Sdk: AccessorySdk,
  Accessory: AccessoryInstance
};
